const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');
const {JWT_SECRET} = require('../config');

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;
  const user = await User.findOne({email});

  if (user) {
    return res.status(400).json({message: `Email ${email} already exists`});
  }
  const newUser = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  await newUser.save();
  res.json({message: 'Profile created successfully!'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});
  const isCorrectPassword = user ? await bcrypt.compare(password,
      user.password) : false;

  if (!user || !isCorrectPassword) {
    return res.status(400).json({message: 'Wrong email or password'});
  }
  const token = jwt.sign({email: user.email, id: user._id}, JWT_SECRET);

  res.json({jwt_token: token});
};

module.exports.getNewPassword = async (req, res) => {
  const user = await User.findOne({email: req.body.email});

  if (!user) {
    return res.status(400).json({message: 'Wrong email'});
  }
  res.json({message: 'New password sent to your email address!'});
};
