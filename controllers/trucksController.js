const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');

module.exports.userRoleChecker = async (req, res, next) => {
  const user = await User.findById(req.user.id);

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }
  if (user.role !== 'DRIVER') {
    return res.status(400).json({message: 'The user is not a driver'});
  }
  next();
};

module.exports.addTruck = async (req, res) => {
  const newTruck = new Truck({
    created_by: req.user.id,
    type: req.body.type,
  });

  await newTruck.save();
  res.json({message: 'Truck created successfully!'});
};

module.exports.getUserTrucks = async (req, res) => {
  const userTrucks = await Truck.find({created_by: req.user.id}, {__v: 0});

  res.json({trucks: userTrucks});
};

module.exports.getUserTruck = async (req, res) => {
  const userTruck = await Truck.findOne(
      {_id: req.params.id, created_by: req.user.id}, {__v: 0});

  if (!userTruck) {
    return res.status(400).json({message: 'No truck found'});
  }
  res.json({truck: userTruck});
};

module.exports.updateUserTruck = async (req, res) => {
  const userTruck = await Truck.findOne(
      {_id: req.params.id, created_by: req.user.id});
  const truckNewType = req.body.type;

  if (!userTruck) {
    return res.status(400).json({message: 'No truck found'});
  }
  if (userTruck.status === 'OL') {
    return res.status(400).json({message: 'The truck is on load'});
  }
  if (truckNewType === userTruck.type) {
    return res.status(400)
        .json({message: 'The new type matches the existing one'});
  }
  await userTruck.updateOne({type: truckNewType});
  res.json({message: 'Truck details changed successfully!'});
};

module.exports.deleteUserTruck = async (req, res) => {
  const userTruck = await Truck.findOne(
      {_id: req.params.id, created_by: req.user.id});

  if (!userTruck) {
    return res.status(400).json({message: 'No truck found'});
  }
  if (userTruck.status === 'OL') {
    return res.status(400).json({message: 'The truck is on load'});
  }
  await userTruck.deleteOne();
  res.json({message: 'Truck deleted successfully!'});
};

module.exports.assignUserTruck = async (req, res) => {
  const userId = req.user.id;
  const userTruck = await Truck.findOne(
      {_id: req.params.id, created_by: userId});
  const truckOnLoad = await Truck.findOne({created_by: userId, status: 'OL'});

  if (!userTruck) {
    return res.status(400).json({message: 'No truck found'});
  }
  if (truckOnLoad) {
    return res.status(400).json({message: 'Active truck found'});
  }
  if (userTruck.assigned_to === userId) {
    return res.status(400).json({message: 'The truck is aready assigned'});
  }
  await Truck.findOneAndUpdate({assigned_to: userId}, {assigned_to: null},
      {new: true});
  await userTruck.updateOne({assigned_to: userId});
  res.json({message: 'Truck assigned successfully!'});
};
