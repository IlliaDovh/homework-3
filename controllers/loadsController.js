const {User} = require('../models/userModel');
const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');

const TRUCK_TYPES = [
  {
    type: 'SPRINTER',
    width: 300,
    length: 250,
    height: 170,
    payload: 1700,
  },
  {
    type: 'SMALL STRAIGHT',
    width: 500,
    length: 250,
    height: 170,
    payload: 2500,
  },
  {
    type: 'LARGE STRAIGHT',
    width: 700,
    length: 350,
    height: 200,
    payload: 4000,
  },
];
const LOAD_STATES = ['En route to Pick Up', 'Arrived to Pick Up',
  'En route to delivery', 'Arrived to delivery'];

module.exports.shipperRoleChecker = async (req, res, next) => {
  const user = await User.findById(req.user.id);

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }
  if (user.role !== 'SHIPPER') {
    return res.status(400).json({message: 'The user is not a shipper'});
  }
  next();
};

module.exports.userExistenceChecker = async (req, res, next) => {
  const user = await User.findById(req.user.id);

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }
  next();
};

module.exports.driverRoleChecker = async (req, res, next) => {
  const user = await User.findById(req.user.id);

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }
  if (user.role !== 'DRIVER') {
    return res.status(400).json({message: 'The user is not a driver'});
  }
  next();
};

module.exports.addLoad = async (req, res) => {
  const {name, payload, pickup_address: pickupAddress,
    delivery_address: deliveryAddress, dimensions} = req.body;
  const newLoad = new Load({
    created_by: req.user.id,
    name: name,
    payload: payload,
    pickup_address: pickupAddress,
    delivery_address: deliveryAddress,
    dimensions: dimensions,
  });

  await newLoad.save();
  res.json({message: 'Load created successfully!'});
};

module.exports.getLoads = async (req, res) => {
  const userId = req.user.id;
  const {status, offset, limit = 10} = req.query;
  let shipperLoadStatuses = /NEW|POSTED|ASSIGNED|SHIPPED/;
  let driverLoadStatuses = /ASSIGNED|SHIPPED/;
  const userRole = (await User.findById(userId)).role;

  if (status) {
    shipperLoadStatuses = status;
    driverLoadStatuses = status;
  }
  if (userRole === 'SHIPPER') {
    const shipperLoads = await Load.find(
        {created_by: userId, status: shipperLoadStatuses},
        {__v: 0},
        {
          skip: parseInt(offset),
          limit: limit > 50 ? 50 : parseInt(limit),
        },
    );

    return res.json({loads: shipperLoads});
  }
  const driverLoads = await Load.find(
      {assigned_to: userId, status: driverLoadStatuses},
      {__v: 0},
      {
        skip: parseInt(offset),
        limit: limit > 50 ? 50 : parseInt(limit),
      },
  );

  res.json({loads: driverLoads});
};

module.exports.getActiveLoad = async (req, res) => {
  const activeLoad = await Load.findOne(
      {assigned_to: req.user.id, status: 'ASSIGNED'}, {__v: 0});

  if (!activeLoad) {
    return res.status(400).json({message: 'No active load found'});
  }
  return res.json({load: activeLoad});
};

module.exports.iterateLoadState = async (req, res) => {
  const activeLoad = await Load.findOne(
      {assigned_to: req.user.id, status: 'ASSIGNED'});

  if (!activeLoad) {
    return res.status(400).json({message: 'No active load found'});
  }
  const activeLoadNextStateIndex = LOAD_STATES.indexOf(activeLoad.state) + 1;

  if (activeLoadNextStateIndex === LOAD_STATES.length - 1) {
    await activeLoad.updateOne(
        {
          status: 'SHIPPED',
          state: LOAD_STATES[activeLoadNextStateIndex],
          $push: {logs:
            {
              message: `Load state changed to
                '${LOAD_STATES[activeLoadNextStateIndex]}'`,
              time: Date.now(),
            },
          },
        },
    );
    await Truck.findOneAndUpdate({assigned_to: req.user.id}, {status: 'IS'},
        {new: true});
    return res.json({message:
        `Load state changed to '${LOAD_STATES[activeLoadNextStateIndex]}'`});
  }
  await activeLoad.updateOne(
      {
        state: LOAD_STATES[activeLoadNextStateIndex],
        $push: {logs:
          {
            message: `Load state changed to
              '${LOAD_STATES[activeLoadNextStateIndex]}'`,
            time: Date.now(),
          },
        },
      },
  );
  return res.json({message:
    `Load state changed to '${LOAD_STATES[activeLoadNextStateIndex]}'`});
};

module.exports.getLoad = async (req, res) => {
  const userId = req.user.id;
  const userRole = (await User.findById(userId)).role;
  const loadId = req.params.id;

  if (userRole === 'SHIPPER') {
    const shipperLoad = await Load.findOne({_id: loadId, created_by: userId},
        {__v: 0});

    if (!shipperLoad) {
      return res.status(400).json({message: 'No load found'});
    }
    return res.json({load: shipperLoad});
  }
  const driverLoad = await Load.findOne({_id: loadId, assigned_to: userId},
      {__v: 0});

  if (!driverLoad) {
    return res.status(400).json({message: 'No load found'});
  }
  res.json({load: driverLoad});
};

module.exports.updateUserLoad = async (req, res) => {
  const userLoad = await Load.findOne(
      {_id: req.params.id, created_by: req.user.id});
  const {name, payload, pickup_address: pickupAddress,
    delivery_address: deliveryAddress, dimensions} = req.body;

  if (!userLoad) {
    return res.status(400).json({message: 'No load found'});
  }
  if (userLoad.status === 'POSTED' || userLoad.status === 'ASSIGNED') {
    return res.status(400).json({message: 'The load is active at the moment'});
  }
  if (userLoad.status === 'SHIPPED') {
    return res.status(400).json({message: 'The load is in history'});
  }
  const userLoadDimensions = userLoad.dimensions;

  if (name === userLoad.name && payload === userLoad.payload &&
    pickupAddress === userLoad.pickup_address &&
    deliveryAddress === userLoad.delivery_address &&
    dimensions.width === userLoadDimensions.width &&
    dimensions.length === userLoadDimensions.length &&
    dimensions.height === userLoadDimensions.height) {
    return res.status(400).json({message: 'New details match existing ones'});
  }
  await userLoad.updateOne(
      {
        name,
        payload,
        pickup_address: pickupAddress,
        delivery_address: deliveryAddress,
        dimensions,
        $push: {logs: {message: 'Load updated', time: Date.now()}},
      },
  );
  res.json({message: 'Load details changed successfully!'});
};

module.exports.deleteUserLoad = async (req, res) => {
  const userLoad = await Load.findOne(
      {_id: req.params.id, created_by: req.user.id});

  if (!userLoad) {
    return res.status(400).json({message: 'No load found'});
  }
  if (userLoad.status === 'POSTED' || userLoad.status === 'ASSIGNED') {
    return res.status(400).json({message: 'The load is active at the moment'});
  }
  if (userLoad.status === 'SHIPPED') {
    return res.status(400).json({message: 'The load is in history'});
  }
  userLoad.deleteOne();
  res.json({message: 'Load deleted successfully!'});
};

module.exports.postUserLoad = async (req, res) => {
  const userLoad = await Load.findOne(
      {_id: req.params.id, created_by: req.user.id});

  if (!userLoad) {
    return res.status(400).json({message: 'No load found'});
  }
  if (userLoad.status === 'POSTED') {
    return res.status(400).json({message: 'The load is already posted'});
  }
  if (userLoad.status === 'ASSIGNED') {
    return res.status(400).json({message: 'The load is active at the moment'});
  }
  if (userLoad.status === 'SHIPPED') {
    return res.status(400).json({message: 'The load is in history'});
  }
  const userLoadDimensions = userLoad.dimensions;
  const userLoadPayload = userLoad.payload;
  let assignedTruck = null;

  await userLoad.updateOne(
      {
        status: 'POSTED',
        $push: {logs: {message: 'Load posted', time: Date.now()}},
      },
  );
  for (let i = 0; i < TRUCK_TYPES.length; i++) {
    if (TRUCK_TYPES[i].width > userLoadDimensions.width &&
      TRUCK_TYPES[i].length > userLoadDimensions.length &&
      TRUCK_TYPES[i].height > userLoadDimensions.height &&
      TRUCK_TYPES[i].payload > userLoadPayload) {
      assignedTruck = await Truck.findOne(
          {type: TRUCK_TYPES[i].type, status: 'IS'})
          .where('assigned_to').ne(null);
      if (assignedTruck) {
        break;
      }
    }
  }
  if (!assignedTruck) {
    await userLoad.updateOne(
        {
          status: 'NEW',
          $push: {logs: {message: 'No truck assigned', time: Date.now()}},
        },
    );
    return res.status(200).json({message: 'No truck assigned'});
  }
  await assignedTruck.updateOne({status: 'OL'});
  await userLoad.updateOne(
      {
        assigned_to: assignedTruck.assigned_to,
        status: 'ASSIGNED',
        state: LOAD_STATES[0],
        $push: {logs:
          {
            message: `Load assigned to driver with id
              ${assignedTruck.assigned_to}`,
            time: Date.now(),
          },
        },
      },
  );
  res.json({message: 'Load assigned successfully!', driver_found: true});
};

module.exports.getUserLoadShippingInfo = async (req, res) => {
  const userLoadInfo = await Load.findOne(
      {_id: req.params.id, created_by: req.user.id}, {__v: 0});

  if (!userLoadInfo) {
    return res.status(400).json({message: 'No load found'});
  }
  if (userLoadInfo.status !== 'ASSIGNED') {
    return res.status(400).json({message: 'The load is not active'});
  }
  const assignedTruckInfo = await Truck.findOne(
      {assigned_to: userLoadInfo.assigned_to}, {__v: 0});

  return res.json({load: userLoadInfo, truck: assignedTruckInfo});
};
