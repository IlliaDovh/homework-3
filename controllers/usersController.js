const bcrypt = require('bcrypt');

const {User} = require('../models/userModel');
const {Load} = require('../models/loadModel');

module.exports.getUserProfileInfo = async (req, res) => {
  const userProfileInfo = await User.findById(req.user.id,
      {password: 0, role: 0, __v: 0});

  if (!userProfileInfo) {
    return res.status(400).json({message: 'No user found'});
  }
  res.json({user: userProfileInfo});
};

module.exports.deleteUserProfile = async (req, res) => {
  const userId = req.user.id;
  const user = await User.findById(userId);
  const userActiveLoad = await Load.findOne(
      {created_by: userId, status: /POSTED|ASSIGNED/});

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }
  if (user.role === 'DRIVER') {
    return res.status(400)
        .json({message: 'Driver profile can not be deleted'});
  }
  if (userActiveLoad) {
    return res.status(400).json({message: 'Active load found'});
  }
  await user.deleteOne();
  await Load.deleteMany({created_by: userId, status: 'NEW'});
  res.json({message: 'Profile deleted successfully!'});
};

module.exports.changeUserPassword = async (req, res) => {
  const user = await User.findById(req.user.id);
  const {oldPassword, newPassword} = req.body;
  const isCorrectPassword = user ? await bcrypt.compare(oldPassword,
      user.password) : false;

  if (!user) {
    return res.status(400).json({message: 'No user found'});
  }
  if (!isCorrectPassword) {
    return res.status(400).json({message: 'Wrong existing password'});
  }
  if (newPassword === oldPassword) {
    return res.status(400)
        .json({message: 'The new password matches the existing one'});
  }
  await user.updateOne({password: await bcrypt.hash(newPassword, 10)});
  res.json({message: 'Password changed successfully!'});
};
