const express = require('express');

const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {shipperRoleChecker, userExistenceChecker, driverRoleChecker, addLoad,
  getLoads, getActiveLoad, iterateLoadState, getLoad, updateUserLoad,
  deleteUserLoad, postUserLoad, getUserLoadShippingInfo} =
  require('../controllers/loadsController');
const {loadValidation} = require('./middlewares/validationMiddleware');

const router = new express.Router();

router.post('/', authMiddleware, asyncWrapper(shipperRoleChecker),
    asyncWrapper(loadValidation), addLoad);
router.get('/', authMiddleware, asyncWrapper(userExistenceChecker), getLoads);
router.get('/active', authMiddleware, asyncWrapper(driverRoleChecker),
    getActiveLoad);
router.patch('/active/state', authMiddleware, asyncWrapper(driverRoleChecker),
    iterateLoadState);
router.get('/:id', authMiddleware, asyncWrapper(userExistenceChecker), getLoad);
router.put('/:id', authMiddleware, asyncWrapper(shipperRoleChecker),
    asyncWrapper(loadValidation), updateUserLoad);
router.delete('/:id', authMiddleware, asyncWrapper(shipperRoleChecker),
    deleteUserLoad);
router.post('/:id/post', authMiddleware, asyncWrapper(shipperRoleChecker),
    postUserLoad);
router.get('/:id/shipping_info', authMiddleware,
    asyncWrapper(shipperRoleChecker), getUserLoadShippingInfo);

module.exports = router;
