const express = require('express');

const {asyncWrapper} = require('./helpers');
const {registrationValidation, loginValidation, forgotPasswordValidation} =
    require('./middlewares/validationMiddleware');
const {registration, login, getNewPassword} =
    require('../controllers/authController');

const router = new express.Router();

router.post('/register', asyncWrapper(registrationValidation),
    asyncWrapper(registration));
router.post('/login', asyncWrapper(loginValidation), asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgotPasswordValidation),
    asyncWrapper(getNewPassword));

module.exports = router;
