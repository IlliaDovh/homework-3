const joi = require('joi');

module.exports.registrationValidation = async (req, res, next) => {
  const schema = joi.object({
    email: joi.string().email().required(),
    password: joi.string().required(),
    role: joi.string().pattern(new RegExp('^(DRIVER|SHIPPER)$')).required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.loginValidation = async (req, res, next) => {
  const schema = joi.object({
    email: joi.string().email().required(),
    password: joi.string().required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.forgotPasswordValidation = async (req, res, next) => {
  const schema = joi.object({
    email: joi.string().email().required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.paswordChangingValidation = async (req, res, next) => {
  const schema = joi.object({
    oldPassword: joi.string().required(),
    newPassword: joi.string().required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.truckValidation = async (req, res, next) => {
  const schema = joi.object({
    type: joi.string().pattern(
        new RegExp('^(SPRINTER|SMALL STRAIGHT|LARGE STRAIGHT)$')).required(),
  });

  await schema.validateAsync(req.body);
  next();
};

module.exports.loadValidation = async (req, res, next) => {
  const schema = joi.object({
    name: joi.string().required(),
    payload: joi.number().required(),
    pickup_address: joi.string().required(),
    delivery_address: joi.string().required(),
    dimensions: {
      width: joi.number().required(),
      length: joi.number().required(),
      height: joi.number().required(),
    },
  });

  await schema.validateAsync(req.body);
  next();
};
