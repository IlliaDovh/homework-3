const express = require('express');

const {authMiddleware} = require('./middlewares/authMiddleware');
const {asyncWrapper} = require('./helpers');
const {userRoleChecker, addTruck, getUserTrucks, getUserTruck, updateUserTruck,
  deleteUserTruck, assignUserTruck} =
  require('../controllers/trucksController');
const {truckValidation} = require('./middlewares/validationMiddleware');

const router = new express.Router();

router.post('/', authMiddleware, asyncWrapper(userRoleChecker),
    asyncWrapper(truckValidation), addTruck);
router.get('/', authMiddleware, asyncWrapper(userRoleChecker), getUserTrucks);
router.get('/:id', authMiddleware, asyncWrapper(userRoleChecker), getUserTruck);
router.put('/:id', authMiddleware, asyncWrapper(userRoleChecker),
    asyncWrapper(truckValidation), updateUserTruck);
router.delete('/:id', authMiddleware, asyncWrapper(userRoleChecker),
    deleteUserTruck);
router.post('/:id/assign', authMiddleware, asyncWrapper(userRoleChecker),
    assignUserTruck);

module.exports = router;
